/*
 Navicat Premium Data Transfer

 Source Server         : 193.112.200.209
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : 193.112.200.209:3306
 Source Schema         : base

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 12/08/2021 15:27:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth
-- ----------------------------
DROP TABLE IF EXISTS `auth`;
CREATE TABLE `auth`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级id',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `m` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'admin' COMMENT '模块名称',
  `path` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应路径',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `sorted` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序，数字越大越靠前',
  `is_show` tinyint UNSIGNED NOT NULL DEFAULT 10 COMMENT '10显示,20不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 87 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of auth
-- ----------------------------
INSERT INTO `auth` VALUES (1, 0, '权限管理', 'admin', '/auth', NULL, 0, 10);
INSERT INTO `auth` VALUES (2, 1, '权限列表', 'admin', '/auth/index', NULL, 0, 10);
INSERT INTO `auth` VALUES (3, 2, '权限添加', 'admin', '/auth/save', NULL, 0, 20);
INSERT INTO `auth` VALUES (4, 2, '权限编辑', 'admin', '/auth/update', NULL, 0, 20);
INSERT INTO `auth` VALUES (5, 2, '权限删除', 'admin', '/auth/delete', NULL, 0, 20);
INSERT INTO `auth` VALUES (6, 1, '角色列表', 'admin', '/role/index', NULL, 0, 10);
INSERT INTO `auth` VALUES (7, 6, '角色添加', 'admin', '/role/save', NULL, 0, 20);
INSERT INTO `auth` VALUES (8, 6, '角色编辑', 'admin', '/role/update', NULL, 0, 20);
INSERT INTO `auth` VALUES (9, 6, '角色删除', 'admin', '/role/delete', NULL, 0, 20);
INSERT INTO `auth` VALUES (10, 1, '管理员列表', 'admin', '/manager/index', NULL, 0, 10);
INSERT INTO `auth` VALUES (11, 10, '管理员添加', 'admin', '/manager/save', NULL, 0, 20);
INSERT INTO `auth` VALUES (12, 10, '管理员编辑', 'admin', '/manager/update', NULL, 0, 20);
INSERT INTO `auth` VALUES (13, 10, '管理员删除', 'admin', '/manager/delete', NULL, 0, 20);

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int NULL DEFAULT NULL COMMENT '操作者id',
  `user_username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作者',
  `ip` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作IP',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求地址',
  `method` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求方法',
  `param` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求参数',
  `create_time` int UNSIGNED NOT NULL COMMENT '请求时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of log
-- ----------------------------
INSERT INTO `log` VALUES (1, NULL, NULL, '127.0.0.1', 'account', 'post', '{\"username\":\"root\",\"password\":\"123456\",\"repassword\":\"123456\"}', 1628751737);
INSERT INTO `log` VALUES (2, NULL, NULL, '127.0.0.1', 'account', 'post', '{\"username\":\"root\",\"password\":\"123456\",\"repassword\":\"123456\"}', 1628752015);

-- ----------------------------
-- Table structure for manager
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `sex` tinyint UNSIGNED NOT NULL COMMENT '性别：10男，20女，30保密',
  `phone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号码',
  `email` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱',
  `status` tinyint UNSIGNED NOT NULL DEFAULT 10 COMMENT '状态：10正常，20禁用',
  `last_login_ip` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后登陆ip',
  `login_times` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '登陆次数',
  `remark` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `create_time` int UNSIGNED NOT NULL COMMENT '创建时间',
  `update_time` int UNSIGNED NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '平台管理员表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES (1, 'root', 'e10adc3949ba59abbe56e057f20f883e', 10, '18676638286', '3539949704@qq.com', 10, '127.0.0.1', 37, '超管', 1623823494, 1628752015);

-- ----------------------------
-- Table structure for manager_role
-- ----------------------------
DROP TABLE IF EXISTS `manager_role`;
CREATE TABLE `manager_role`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `manager_id` int UNSIGNED NOT NULL COMMENT '用户id',
  `role_id` int UNSIGNED NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '平台管理员-角色关系表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of manager_role
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_desc` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '角色描述',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role
-- ----------------------------

-- ----------------------------
-- Table structure for role_auth
-- ----------------------------
DROP TABLE IF EXISTS `role_auth`;
CREATE TABLE `role_auth`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `auth_id` int UNSIGNED NOT NULL COMMENT '权限id',
  `role_id` int UNSIGNED NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色-权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role_auth
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
