<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/8/9
 * Time: 10:17
 */

namespace app\service;


use app\model\MessageBoardModel;

class MessageBoardService
{
    protected $model;

    public function __construct()
    {
        $this->model = new MessageBoardModel();
    }

    /**
     * 留言添加
     * @param $data
     * @return bool
     */
    public function save($data) {
        return $this->model->save($data);
    }

    /**
     * 留言查询
     * @param $where
     * @return \think\Paginator
     * @throws \think\db\exception\DbException
     */
    public function index($where) {
        $rows = $this->model->field('id,name,wechat,email,type,content,status,create_time,update_time')
            ->where('status', '<>', 30)
            ->order(['status' => 'ASC', 'create_time' => 'ASC'])
            ->paginate($where['list_rows']);
        return $rows;
    }

    /**
     * 留言修改
     * @param $where
     * @param $data
     * @return mixed
     */
    public function update($where, $data) {
        return $this->model->where($where)->update($data);
    }

    /**
     * 留言删除
     * @param $id
     * @return mixed
     */
    public function delete($id) {
        return $this->update(['id' => $id], ['status' => 30]);
    }
}
