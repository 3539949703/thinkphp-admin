<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/18
 * Time: 9:29
 */

namespace app\model;


use think\Model;

class ManagerRoleModel extends Model
{
    protected $name = "manager_role";
}