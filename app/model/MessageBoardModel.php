<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/8/9
 * Time: 10:08
 */

namespace app\model;


use think\Model;

class MessageBoardModel extends Model
{
    protected $name = "message_board";
}