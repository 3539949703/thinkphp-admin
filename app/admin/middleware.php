<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/18
 * Time: 15:30
 */
return [
    // 认证
    app\admin\middleware\Check::class,
    // 授权
    app\admin\middleware\Auth::class,
    // 入参校验
    app\admin\middleware\ParamValidate::class,
    // 操作日志记录
    app\admin\middleware\Log::class
];