<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/17
 * Time: 11:58
 */

namespace app\admin\controller;


use app\admin\service\AuthService;
use app\BaseController;

class Auth extends BaseController
{
    /**
     * @var $service AuthService
     */
    protected $service;

    public function initialize()
    {
        parent::initialize();
        $this->service = new AuthService();
    }

    /**
     * 查询权限
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index() {
        $rows = $this->service->findAll();
        return success('ok', getTree($rows));
    }

    /**
     * 添加权限
     * @return \think\response\Json
     */
    public function save() {
        $result = $this->service->save(request()->data);  // 成功true，失败false
        if ($result === true) {
            return success('创建成功');
        }
        return fail('创建失败');
    }

    /**
     * 编辑权限
     * @param $id
     * @return \think\response\Json
     */
    public function update($id) {
        $row = $this->service->update($id, request()->data);
        if ($row === 0) {
            return fail('修改失败');
        }
        return success('修改成功');
    }

    /**
     * 删除权限
     * @param $id
     * @return \think\response\Json
     */
    public function delete($id) {
        $row = $this->service->delete($id);
        if ($row === 0) {
            return fail('删除失败');
        }
        return success('删除成功');
    }
}