<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/16
 * Time: 8:45
 */

namespace app\admin\controller;

use app\admin\service\RoleService;
use app\BaseController;

class Role extends BaseController
{
    /**
     * @var $service RoleService
     */
    protected $service;

    public function initialize()
    {
        parent::initialize();
        $this->service = new RoleService();
    }

    /**
     * 查询数据
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     */
    public function index() {
        $data = request()->data;
        // 参数默认处理
        $data['page'] = empty($data['page']) ? 1 : $data['page'];
        $data['list_rows'] = empty($data['list_rows']) ? 15 : $data['list_rows'];
        $result = $this->service->findAll($data);
        return success('ok', $result);
    }

    /**
     * 编辑角色
     * @param $id
     * @return \think\response\Json
     */
    public function update($id) {
        $row = $this->service->update($id, request()->data);
        if ($row === 0) {
            return fail('修改失败');
        }
        return success('修改成功');
    }

    /**
     * 添加角色
     * @return \think\response\Json
     */
    public function save() {
        $result = $this->service->save(request()->data);  // 成功true，失败false
        if ($result === true) {
            return success('创建成功');
        }
        return fail('创建失败');
    }

    /**
     * 删除角色
     * @param $id
     * @return \think\response\Json
     */
    public function delete($id) {
        $row = $this->service->delete($id);
        if ($row === 0) {
            return fail('删除失败');
        }
        return success('删除成功');
    }
}