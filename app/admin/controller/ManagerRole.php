<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/18
 * Time: 9:31
 */

namespace app\admin\controller;


use app\admin\service\ManagerRoleService;
use app\BaseController;

class ManagerRole extends BaseController
{
    /**
     * @var $service ManagerRoleService
     */
    protected $service;

    public function initialize()
    {
        parent::initialize();
        $this->service = new ManagerRoleService();
    }

    /**
     * 通过manager_id获取role_id
     * @param $id
     * @return \think\response\Json
     */
    public function read($id) {
        $row = $this->service->findOne($id);
        return success('ok', $row);
    }

    /**
     * 给用户设置角色
     * @param $id
     * @return \think\response\Json
     */
    public function update($id) {
        if ($id == 1) {
            return fail('超管不需要设置角色');
        }
        $row = $this->service->update($id, request()->data->role_id);
        if ($row == 1) {
            return success('设置成功');
        } else {
            return fail('设置失败');
        }
    }
}