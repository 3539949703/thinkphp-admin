<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/8/9
 * Time: 10:22
 */

namespace app\admin\controller;


use app\BaseController;
use app\service\MessageBoardService;

class MessageBoard extends BaseController
{
    /**
     * @var $service MessageBoardService
     */
    protected $service;

    public function initialize()
    {
        parent::initialize();
        $this->service = new MessageBoardService();
    }

    /**
     * 留言查询
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     */
    public function index() {
        // 参数默认处理
        $params = request()->data;
        $params['page'] = empty($params['page']) ? 1 : $params['page'];
        $params['list_rows'] = empty($params['list_rows']) ? 15 : $params['list_rows'];
        $res = $this->service->index($params);
        return success('ok', $res);
    }

    /**
     * 留言修改
     * @param $id
     * @return \think\response\Json
     */
    public function update($id) {
        $row = $this->service->update(['id' => $id], request()->data);
        if ($row == 1) {
            return success('修改成功');
        }
        return fail('修改失败');
    }

    /**
     * 留言删除
     * @param $id
     * @return \think\response\Json
     */
    public function delete($id) {
        $row = $this->service->delete($id);
        if ($row == 1) {
            return success('删除成功');
        }
        return fail('删除失败');
    }
}