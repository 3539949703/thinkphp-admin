<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/16
 * Time: 8:45
 */

namespace app\admin\controller;

use app\admin\service\ManagerService;
use app\BaseController;
use think\facade\Cache;

class Manager extends BaseController
{
    /**
     * @var $service ManagerService
     */
    protected $service;

    public function initialize()
    {
        parent::initialize();
        $this->service = new ManagerService();
    }

    /**
     * 查询管理员
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     */
    public function index() {
        // 参数默认处理
        $data = request()->data;
        $data['page'] = empty($data['page']) ? 1 : $data['page'];
        $data['list_rows'] = empty($data['list_rows']) ? 15 : $data['list_rows'];
        $result = $this->service->findAll($data);
        return success('ok', $result);
    }

    /**
     * 编辑管理员
     * @param $id
     * @return \think\response\Json
     */
    public function update($id) {
        $data = request()->data;
        if (!empty($data['password'])) {
            $salt = randString(8);
            $data['salt'] = $salt;
            $data['password'] = md5($data['password'] . $salt);
        }
        $row = $this->service->update($id, $data);
        if ($row === 1) {
            return success('修改成功');

        }
        return fail('修改失败');
    }

    /**
     * 添加管理员
     * @return \think\response\Json
     */
    public function save() {
        $data = request()->data;
        // 服务端数据补充
        if (!empty($data['password'])) {
            $salt = randString(8);
            $data['salt'] = $salt;
            $data['password'] = md5($data['password'] . $salt);
        }
        // 数据写入
        $result = $this->service->save($data);  // 成功true，失败false
        if ($result === true) {
            return success('创建成功');
        }
        return fail('创建失败');
    }

    /**
     * 平台账户删除
     * @param $id
     * @return \think\response\Json
     */
    public function delete($id) {
        $row = $this->service->delete($id);
        if ($row === 1) {
            Cache::delete('token_admin_' . $id);
            return success('删除成功');
        }
        return fail('删除失败');

    }
}