<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/17
 * Time: 18:52
 */

namespace app\admin\service;


use app\model\RoleAuthModel;

class RoleAuthService
{
    protected $model;

    public function __construct()
    {
        $this->model = new RoleAuthModel();
    }

    /**
     * 通过角色id获取全部权限id
     * @param $role_id
     * @return array
     */
    public function getAuthIdByRoleId($role_id) {
        return  $this->model->where('role_id', 'in', $role_id)->column('auth_id');
    }

    /**
     * 给角色设置权限
     * @param $roleId
     * @param $authIds
     */
    public function setAuthToRole($roleId, $authIds) {
        $data = [];
        foreach ($authIds as $authId) {
            $temp = ['role_id' => $roleId, 'auth_id' => $authId];
            $data[] = $temp;
        }
        $this->model->insertAll($data);
    }

    /**
     * 给角色删除权限
     * deleteAuthToRole
     * @author: buddha
     * @date 2020/8/7 17:57
     * @param $roleId
     * @param $authIds
     */
    public function deleteAuthToRole($roleId, $authIds)
    {
        foreach ($authIds as $authId) {
            $where = ['role_id' => $roleId, 'auth_id' => $authId];
            $this->model->where($where)->delete();
        }
    }
}