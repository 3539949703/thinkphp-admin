<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/16
 * Time: 10:26
 */

use think\facade\Route;

Route::resource('manager', 'Manager');
Route::resource('role', 'Role');
Route::resource('auth', 'Auth');
Route::resource('roleAuth', 'RoleAuth');
Route::resource('managerRole', 'ManagerRole');
Route::resource('account', 'Account');
Route::resource('menu', 'Menu');
Route::resource("messageBoard", "MessageBoard");

Route::miss(function () {
    return json(['status' => 1, 'message' => '接口不存在', 'data' => null]);
});