<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/7/9
 * Time: 11:13
 */

namespace app\admin\middleware;


use app\model\LogModel;

class Log
{
    public function handle($request, \Closure $next) {
        $method = strtolower(request()->method());
        if (in_array($method, ['post', 'put', 'delete'])) {
            $data = ['url' => request()->pathinfo(), 'method' => $method, 'create_time' => time(), 'ip' => getIp()];
            if (!empty(request()->param())) {
                $data['param'] = json_encode(request()->param());
            }
            if (!empty(request()->userInfo)) {
                $data['user_id'] = request()->userInfo->id;
                $data['user_username'] = request()->userInfo->username;
            }
            (new LogModel())->save($data);
        }
        return $next($request);
    }
}