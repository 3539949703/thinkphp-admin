<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/16
 * Time: 10:26
 */

use think\facade\Route;

Route::resource("messageBoard", "MessageBoard");

Route::miss(function () {
    return json(['status' => 1, 'message' => '接口不存在', 'data' => null]);
});