<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/8/9
 * Time: 10:22
 */

namespace app\index\controller;


use app\BaseController;
use app\service\MessageBoardService;

class MessageBoard extends BaseController
{
    /**
     * @var $service MessageBoardService
     */
    protected $service;

    public function initialize()
    {
        parent::initialize();
        $this->service = new MessageBoardService();
    }

    /**
     * 留言添加
     * @return \think\response\Json
     */
    public function save() {
        $row = $this->service->save(request()->data);
        if ($row == true) {
            return success('新增成功');
        }
        return fail('新增失败');
    }
}