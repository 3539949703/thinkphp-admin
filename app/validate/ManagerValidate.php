<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/15
 * Time: 10:28
 */

namespace app\validate;


use think\Validate;

class ManagerValidate extends Validate
{
    protected $rule = [
        'username' => 'length:3,32',
        'password' => 'length:3,32',
        'sex' => 'number|in:10,20,30',
        'phone' => 'mobile',
        'email' => 'email',
        'status' => 'number|in:10,20',
        'page' => 'number',
        'list_rows' => 'number',
    ];

    protected $message = [
        'username.length' => '账号长度3-32字符',
        'username.unique' => '账号不能重复',
        'password.length' => '密码长度3-32字符',
        'sex.number' => '性别是数字',
        'sex.in' => '性别非法',
        'phone' => '手机号码非法',
        'email' => '邮箱非法',
        'status.number' => '状态是数字',
        'status.in' => '状态非法',
        'page.number' => '当前页是数字',
        'list_rows.number' => '每页数量是数字'
    ];

    public $scene = [
        // 创建账户场景
        'save' => ['username', 'password', 'sex', 'phone', 'email', 'remark'],
        // 查询账户场景
        'index' => ['page', 'list_rows'],
        // 编辑场景
        'update' => ['sex', 'phone', 'email', 'status', 'remark', 'password'],
    ];
}