<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/17
 * Time: 11:41
 */

namespace app\validate;


use think\Validate;

class AuthValidate extends Validate
{
    protected $rule = [
        'id' => 'require|integer',
        'pid' => 'integer',
        'name' => 'length:3,32',
        'm' => 'length:1,32',
        'path' => 'length:1,128',
        'icon' => 'length:1,255',
        'sorted' => 'number',
        'is_show' => 'number|in:10,20',
        'page' => 'number',
        'list_rows' => 'number'
    ];

    protected $message = [
        'id.require' => '权限id必须',
        'id.integer' => '权限id是整数',
        'pid.integer' => '父级id是整数',
        'm.length' => '模块名称长度1-32字符',
        'path.length' => '对应路径长度1-32字符',
        'icon.length' => '图标名称长度1-32字符',
        'is_show.number' => '状态是数字',
        'is_show.in' => '状态非法',
        'page.number' => '当前页是数字',
        'list_rows.number' => '每页数量是数字'
    ];

    public $scene = [
        // 权限创建
        'save' => ['pid', 'name', 'm', 'path', 'icon', 'sorted', 'is_show'],
        // 权限查询
        'index' => ['page', 'list_rows'],
        // 权限编辑
        'update' => ['pid', 'name', 'm', 'path', 'icon', 'sorted', 'is_show']
    ];
}