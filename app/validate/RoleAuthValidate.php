<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/16
 * Time: 16:11
 */

namespace app\validate;

use think\Validate;

class RoleAuthValidate extends Validate
{
    protected $rule = [
        'auth_id' => 'min:1',
        'page' => 'number',
        'list_rows' => 'number',
    ];

    protected $message = [
        'auth_id.min' => 'auth_id至少1个权限',
        'page.number' => '当前页是数字',
        'list_rows.number' => '每页数量是数字'
    ];

    public $scene = [
        'update' => ['auth_id'],
    ];
}
