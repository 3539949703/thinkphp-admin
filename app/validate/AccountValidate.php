<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/8/12
 * Time: 14:04
 */

namespace app\validate;


use think\Validate;

class AccountValidate extends Validate
{
    public $rule = [
        'username' => 'require|length:3,32',
        'password' => 'require|length:3,32',
    ];

    public $message = [
        'username.require' => '账号必须',
        'username.length' => '账号长度3-32字符',
        'password.require' => '密码必须',
        'password.length' => '密码长度3-32字符',
    ];

    public $scene = [
        // 登陆
        'save' => ['username', 'password'],
    ];
}