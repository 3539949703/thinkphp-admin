<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/15
 * Time: 10:28
 */

namespace app\validate;


use think\Validate;

class ManagerRoleValidate extends Validate
{
    protected $rule = [
        'role_id' => 'min',
        'page' => 'number',
        'list_rows' => 'number',
    ];

    protected $message = [
        'role_id.min' => 'role_id至少为1个角色',
        'page.number' => '当前页是数字',
        'list_rows.number' => '每页数量是数字'
    ];

    public $scene = [
        // 查询账户场景
        'index' => ['page', 'list_rows'],
        // 编辑场景
        'update' => ['role_id'],
    ];
}