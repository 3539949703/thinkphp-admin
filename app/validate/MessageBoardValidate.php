<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/8/9
 * Time: 10:09
 */

namespace app\validate;


use think\Validate;

class MessageBoardValidate extends Validate
{
    protected $rule =   [
        'name' => 'length:1,64',
        'wechat' => 'length:1,64',
        'email' => 'email',
        'type' => 'in:10,20',
        'content' => 'min:10',
        'status' => 'in:10,20,30',
        'page' => 'integer',
        'list_rows' => 'integer'
    ];

    protected $message  =   [
        'name.length' => '称呼字符在1-64字符之间',
        'wechat.length' => '微信号码字符在1-64字符之间',
        'email.email' => '邮箱格式不合法',
        'type.in' => '留言类型不在范围内',
        'content.min' => '留言内容至少10个字符',
        'status.in' => '留言状态不在范围内',
        'page.integer' => 'page是整数',
        'list_rows.integer' => 'list_rows是整数',
    ];

    public $scene = [
        'save' => ['name', 'wechat', 'email', 'type', 'content'],
        'update' => ['status'],
        'index' => ['page', 'list_rows'],
    ];
}