<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/16
 * Time: 16:11
 */

namespace app\validate;

use think\Validate;

class RoleValidate extends Validate
{
    protected $rule = [
        'id' => 'require|integer',
        'name' => 'length:2,32|unique:role',
        'role_desc' => 'chsAlphaNum',
        'page' => 'number',
        'list_rows' => 'number',
    ];

    protected $message = [
        'id.require' => '角色id必须',
        'id.integer' => '角色id是整数',
        'name.length' => '角色名称长度2-32字符',
        'name.unique' => '角色名称不能重复',
        'role_desc.chsAlphaNum' => '角色描述只能是汉字、字母和数字',
        'page.number' => '当前页是数字',
        'list_rows.number' => '每页数量是数字'
    ];

    public $scene = [
        // 创建账户场景
        'save' => ['name', 'role_desc'],
        // 查询账户场景
        'index' => ['page', 'list_rows'],
        // 编辑场景
        'update' => ['name', 'role_desc'],
    ];
}