<?php
/**
 * Created by PhpStorm.
 * User: buddha
 * Date: 2021/6/18
 * Time: 15:03
 */

return [
    'iat' => time(),
    'nbf' => time(),  // 获取0s后可用
    'exp' => 3600 * 48 + time(),
    'iss' => 'http://www.buddha.com',  // 签发者 可选
    'aud' => 'http://www.buddha.com',  // 接收该JWT的一方，可选
    'key' => 'buddha'  // 公钥
];
