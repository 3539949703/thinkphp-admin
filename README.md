ThinkPHP 6.0
===============

> 运行环境要求PHP7.1+，兼容PHP8.0。

## 安装
### 开发环境准备
> phpStudy环境安装，并创建好网站

### 代码克隆到当前目录下
~~~
# 代码克隆到当前目录下
https://gitee.com/3539949703/thinkphp-admin.git .
~~~

### 安装项目所需依赖
~~~
composer install
~~~

## 文档

[完全开发手册](https://www.kancloud.cn/manual/thinkphp6_0/content)

## 实现功能
```$xslt
1. 实现用JWT生成token,并在系统中登陆及退出

2. 解决前后端跨域问题

3. 前后端分离，抛出异常，接口方式返回信息

4. 实现接口参数统一校验

5. 实现RABC权限管理及认证与授权

6. 实现增、改、删日志写入数据库

7. 实现token存redis与存文件切换

8. 实现用户单点登陆
```
